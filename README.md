# BTC client

React Native repository for BTC lib and transaction (WIP)

## Libraries used

- [react-native-crypto][lib1] - React Native alternative from crypto library
- [bip32][lib2] - Derivating path
- [bitcoinjs-lib][lib3] - Transactions and getting public / private key + address
- [react-native-bip39][lib4] - Mnemonic

## Setup

### Install libraries

```
npm i –save react-native-crypto
npm i –save bitcoinjs-lib
npm i –save bip32
npm i –save react-native-bip39
```

### Make these modifications or follow [this][link1]

#### Install

```
npm i -S bitcoinjs-lib react-native-randombytes buffer-reverse buffer@5
npm i -D rn-nodeify
./node_modules/.bin/rn-nodeify --install buffer,stream,assert,events --hack && npm i -S buffer@5
react-native link react-native-randombytes
```

#### Modify in Transaction Builder

> Open transaction_builder.js in .../bitcoinjs-lib/src/transaction_builder.js and require the following: var bufferReverse = require('buffer-reverse')
>
> Locate the TransactionBuilder.prototype.addInput function in transaction_builder.js and replace txHash = Buffer.from(txHash, 'hex').reverse() with the following:
>
> txHash = bufferReverse(new Buffer(txHash, 'hex'))

#### Modify in sjcl

> H=require("cryto") to
>
> H=require("react-native-crypto")
>
> Then there might be an error for asn1.js will be produced: vm cannot be found
>
>  ``` npm install --save vm ```

### If BASE_MAP.fill(255) error

- go in node-modules/base-x/index.js
- change : //BASE_MAP.fill(255)
- insert this :

```
for (let i = 0; i < 256; i++) {
    BASE_MAP[i] = 255
}
```

## Usage (get from a specified path)

- ``` bip39.generateMnemonic(256) ```
Is generating a random seed (24 words). The method is asynchronous

- ``` bip32.fromSeed(new Buffer(seed, 'hex')) ```
creates the root

- ``` path = "m/44'/0'/0'/0/0" ```
defines the specific path and coin

- ``` root.derivePath(path) ```
set the root to the wanted path

- ``` publicKey = derived.publicKey.toString('hex') ```
gives the public key

- ``` { address } = bitcoin.payments.p2pkh({ pubkey: derived.publicKey }) ```
gives the address

- ``` privKey = bitcoin.ECPair.fromPrivateKey(derived.privateKey).toWIF() ```
gives the private key

## Generate random keys

- ``` bitcoin.networks.bitcoin ```
gives the network (main or test)

- ``` keyPair = bitcoin.ECPair.makeRandom({ network: bitcoinNetwork }) ```
creates the keyPair

- ``` address = bitcoin.payments.p2pkh({ pubkey: keyPair.publicKey }).address ```
gives the address

- ``` publicKey = keyPair.publicKey.toString('hex') ```
gives the public key

- ``` privKey = keyPair.toWIF() ```
gives the private key

[lib1]: https://github.com/tradle/react-native-crypto
[lib2]: https://github.com/bitcoinjs/bip32
[lib3]: https://github.com/bitcoinjs/bitcoinjs-lib
[lib4]: https://github.com/novalabio/react-native-bip39#readme
[link1]: https://github.com/bitcoinjs/bitcoinjs-lib/issues/976