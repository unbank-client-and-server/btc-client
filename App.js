/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import "./shim";
import crypto from 'react-native-crypto';
import bip32 from 'bip32';
import bitcoin from 'bitcoinjs-lib';
import bip39 from 'react-native-bip39';
// import hdkey from "ethereumjs-wallet/hdkey";

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};

type State = {
  mnemonic: Array<string>,
  seed: any
};

export default class App extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      mnemonic: [],
      seed: []
    };
  }


  componentDidMount() {
    const _self = this;
    bip39.generateMnemonic(256).then(function(mnemonic) {


      const seed = bip39.mnemonicToSeed( "muscle skate dawn remember pumpkin foil vacuum such brass grass bullet shoulder" ).toString( 'hex' );

      _self.setState({mnemonic: mnemonic, seed: seed});

      // BTC
      const root = bip32.fromSeed(new Buffer(seed, 'hex'));

      const path = "m/44'/0'/0'/0/0";

      const derived = root.derivePath(path);

      const publicKey = derived.publicKey.toString('hex');

      const { address } = bitcoin.payments.p2pkh({ pubkey: derived.publicKey });

      const privKey = bitcoin.ECPair.fromPrivateKey(derived.privateKey).toWIF();

      console.warn("publicKey : " + publicKey);

      console.warn("address : " + address);

      console.warn("privKey : " + privKey);



      // ETH
      const path2 = "m/44'/60'/0'/0/0";
      const derived2 = root.derivePath(path2);
      const publicKeyETH = derived2.publicKey.toString('hex');
      const test = bip32.fromSeed(seed).publicKey.toString("hex");
      // const hdwallet = hdkey.fromMasterSeed(seed);
      // const wallet = hdwallet.derivePath(path2).getWallet();
      // const addressETH = "0x" + wallet.getAddress().toString("hex");
      //
      console.warn("publicKeyETH : 0x" + publicKeyETH);
      //
      console.warn("addressETH : " + test);

      // console.warn("privKeyETH : " + privKey);

      // RANDOM
      // the default is bitcoin... so this is useless though.
      // const bitcoinNetwork = bitcoin.networks.bitcoin;
      //
      // const keyPair = bitcoin.ECPair.makeRandom({ network: bitcoinNetwork });
      //
      // const address2 = bitcoin.payments.p2pkh({ pubkey: keyPair.publicKey }).address;
      //
      // const publicKey2 = keyPair.publicKey.toString('hex');
      //
      // const privKey2 = keyPair.toWIF();
      //
      // console.warn("publicKey : " + publicKey2);
      //
      // console.warn("address : " + address2);
      //
      // console.warn("privKey : " + privKey2);

    }).catch(function(error) {
      console.warn('There has been a problem with your fetch operation: ' + error.message);
      // ADD THIS THROW error
      throw error;
    });
  }

  render() {
    return (

      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{this.state.mnemonic}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
